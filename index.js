var Hapi = require('hapi');
var Good = require('good');
var Boom = require('boom');
var server = new Hapi.Server();

server.connection({
  port: 3000,
  routes: {
    cors: true
  }
});

server.route({
  method: 'GET',
  path: '/api/test',
  handler: function (request, reply) {
    
    var json = {
      'test1':1,
      'test2':2,
      'test3':'three'
    };
    
    reply({data:json});
  }
});

server.register({
  register: Good,
  options: {
    reporters: [{
      reporter: require('good-console'),
      args:[{ log: '*', response: '*' }]
    }]
  }
}, function (err) {
  if (err) {
    throw err;
  }

  server.start(function () {
    server.log('info', 'Server running at: ' + server.info.uri);
  });
});